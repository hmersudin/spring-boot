package com.amigoscode.springboot.student;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

/**
 * Service Layer => mainly responsible for a business logic...
 *
 *  @Component  -> we can use also this annotation, but to be more specific we use @Service
 *                 as this is StudentService class
 *              -> (they are the same thing) => more for a semantic reasons
 */
@Service
public class StudentService {
    /**
     * In order for this method to be served as an ENDPOINT we have to annotate it with @GetMapping
     *
     * GetMapping => we want to get something from our server
     *
     * This method will serve a rest endpoint (annotated with @GetMapping)
     * @return JSON object (list of Student objects).
     */

    /**
     * We moved this method from StudentControllerAPI to be used within the service
     * @return
     */
    @GetMapping
    public List<Student> getStudentsEndpointFromStudentService() {
        return List.of(
                new Student(
                        1L,
                        "Mariam",
                        "mariam.jamal@gmail.com",
                        LocalDate.of(2000, Month.JANUARY,5),
                        22
                )
        );
    }
}
