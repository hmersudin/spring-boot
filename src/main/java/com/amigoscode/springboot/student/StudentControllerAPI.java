package com.amigoscode.springboot.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

/**
 *  -> API Layer (Controller/API)
 *      -> Service Layer (Service)
 *          -> Data Access Layer (DTO)
 *
 * API Layer -> Service Layer => should take with the Service layer in order to get some data
 * Service Layer -> Data Access Layer (DTO) => should talk with the Data Access layer in order to get data
 *
 * User -> API -> Service -> DTO
 * DTO -> Service -> API -> User
 */

/**
 * @RestController => Makes this class to serve REST endpoints (ENDPOINT).
 * (path = "api/v1/student") => instead of localhost:8080 we want localhost:8080/api/v1/student
 */
@RestController
@RequestMapping(path = "api/v1/student")
public class StudentControllerAPI {
    // Here we are going to have a reference to a StudentService
    /**
     * We must instantiate this studentService
     *      -> done by Dependency Injection (@Autowired)
     * in order to getStudentEndpoint
     *
     * Also we must say this StudentService will be instantiated class
     *      -> add @Service in the StudentService class.
     */
    private final StudentService studentService;

    /**
     * This initially did not work, because we do not have an instance of a StudentService.
     * => this.studentService must be injected to studentService within constructor.
     *
     * In order to solve this we will use Dependency Injection
     * @param studentService
     */
    @Autowired
    public StudentControllerAPI(StudentService studentService) {
        this.studentService = studentService;
    }

    /**
     * In order for this method to be served as an ENDPOINT we have to annotate it with @GetMapping
     *
     * GetMapping => we want to get something from our server
     *
     * This method will serve a rest endpoint (annotated with @GetMapping)
     * @return JSON object (list of Student objects).
     */

    /**
     * Implementation of this method was moved to StudentService, and here we are catching it with
     * reference to a StudentService.
     *
     * Using ntier pattern design.
     * @return
     */
    @GetMapping
    public List<Student> getStudentsEndpoint() {
        return studentService.getStudentsEndpointFromStudentService();
    }
}
